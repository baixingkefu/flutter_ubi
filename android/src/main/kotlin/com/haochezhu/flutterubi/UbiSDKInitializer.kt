package com.haochezhu.flutterubi

import android.util.Log
import com.blankj.utilcode.util.ThreadUtils
import com.haochezhu.ubm.api.UBICredentialProvider
import com.haochezhu.ubm.api.UBITracking
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.embedding.engine.dart.DartExecutor
import io.flutter.embedding.engine.loader.ApplicationInfoLoader
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.view.FlutterCallbackInformation
import java.util.concurrent.CountDownLatch

/**
 * Created by xian on 2022/4/22.
 */
class UbiSDKInitializer private constructor() : MethodChannel.MethodCallHandler {

    companion object {
        private var instance: UbiSDKInitializer? = null

        fun getInstance(): UbiSDKInitializer {
            if (instance == null) {
                synchronized(UbiSDKInitializer::class.java) {
                    if (instance == null) {
                        instance = UbiSDKInitializer()
                    }
                }
            }
            return instance!!
        }
    }

    private var flutterEngine: FlutterEngine? = null
    private var methodChannel: MethodChannel? = null

    private val callbackDispatcherHandle = FlutterUbiPreferences.getCallbackDispatcherHandle()
    private val refreshPassportTokenHandle = FlutterUbiPreferences.getRefreshPassportTokenHandle()

    internal fun init() {
        if (callbackDispatcherHandle == 0L && refreshPassportTokenHandle == 0L) {
            Log.w(
                FlutterUbiPlugin.TAG,
                "[UbiSDKInitializer init] Failed to initialize FlutterUbiPlugin because CallbackHandle don't exists"
            )
            return
        }

        startBackgroundIsolate()
    }

    private fun startBackgroundIsolate() {
        if (flutterEngine != null) {
            Log.d(FlutterUbiPlugin.TAG, "[UbiSDKInitializer init] FlutterEngine already created")
            return
        }
        val flutterEngine = FlutterEngine(FlutterUbiPlugin.applicationContext)
        val dartExecutor = flutterEngine.dartExecutor
        methodChannel = MethodChannel(dartExecutor, "flutter_ubi_refresh_passport_token").apply {
            setMethodCallHandler(this@UbiSDKInitializer)
        }
        val callbackInfo =
            FlutterCallbackInformation.lookupCallbackInformation(callbackDispatcherHandle)
        if (callbackInfo == null) {
            Log.e(
                FlutterUbiPlugin.TAG,
                "[UbiSDKInitializer startBackgroundIsolate] Fatal: failed to find callback: $callbackDispatcherHandle"
            )
            return
        }
        val flutterApplicationInfo = ApplicationInfoLoader.load(FlutterUbiPlugin.applicationContext)
        val dartCallback = DartExecutor.DartCallback(
            FlutterUbiPlugin.applicationContext.assets,
            flutterApplicationInfo.flutterAssetsDir,
            callbackInfo
        )
        dartExecutor.executeDartCallback(dartCallback)
    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        Log.d(
            FlutterUbiPlugin.TAG,
            "[UbiSDKInitializer onMethodCall] method: ${call.method}, args: ${call.arguments}"
        )
        when (call.method) {
            "initialized" -> {
                onMethodChannelInitialized()
                result.success("")
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    private fun onMethodChannelInitialized() {
        methodChannel!!.invokeMethod(
            "refreshPassportToken",
            listOf(refreshPassportTokenHandle),
            object : MethodChannel.Result {
                override fun success(result: Any?) {
                    val userPassportToken = result as Map<*, *>?
                    val userId = userPassportToken?.get("userId") as String?
                    val passportToken = userPassportToken?.get("passportToken") as String?
                    Log.d(
                        FlutterUbiPlugin.TAG,
                        "[UbiSDKInitializer onMethodChannelInitialized] Invoke refreshPassportToken success, userId: $userId, passportToken: $passportToken"
                    )
                    if (!userId.isNullOrEmpty() && !passportToken.isNullOrEmpty()) {
                        invokeEnableSDK(userId, passportToken)
                    }
                }

                override fun error(
                    errorCode: String,
                    errorMessage: String?,
                    errorDetails: Any?
                ) {
                    Log.e(
                        FlutterUbiPlugin.TAG,
                        "[UbiSDKInitializer onMethodChannelInitialized] Invoke refreshPassportToken error, code: $errorCode, message: $errorMessage, details: $errorDetails"
                    )
                }

                override fun notImplemented() {
                    Log.d(FlutterUbiPlugin.TAG, "[UbiSDKInitializer onMethodChannelInitialized] Invoke refreshPassportToken error, notImplemented")
                }
            }
        )
    }

    private fun invokeEnableSDK(userId: String, passportToken: String) {
        UBITracking.enableSDK(
            userId,
            createUBICredentialProvider(passportToken),
            onSuccess = {
                Log.d(
                    FlutterUbiPlugin.TAG,
                    "[UbiSDKInitializer invokeEnableSDK] EnableSDK successful"
                )
            },
            onFailure = { code, msg ->
                Log.e(
                    FlutterUbiPlugin.TAG,
                    "[UbiSDKInitializer invokeEnableSDK] EnableSDK failed, code: $code, message: $msg"
                )
            })
    }

    private fun createUBICredentialProvider(passportToken: String): UBICredentialProvider {
        return object : UBICredentialProvider(passportToken) {
            override fun refreshPassportToken(): String {
                Log.d(
                    FlutterUbiPlugin.TAG,
                    "[UBICredentialProvider refreshPassportToken] Refresh passport token"
                )
                val countDownLatch = CountDownLatch(1)
                var newPassportToken = ""
                ThreadUtils.runOnUiThread {
                    methodChannel?.invokeMethod(
                        "refreshPassportToken",
                        listOf(refreshPassportTokenHandle),
                        object : MethodChannel.Result {
                            override fun success(result: Any?) {
                                val userPassportToken = result as Map<*, *>
                                newPassportToken = userPassportToken["passportToken"] as String
                                Log.d(
                                    FlutterUbiPlugin.TAG,
                                    "[UBICredentialProvider refreshPassportToken] success, passportToken: $newPassportToken"
                                )
                                countDownLatch.countDown()
                            }

                            override fun error(
                                errorCode: String,
                                errorMessage: String?,
                                errorDetails: Any?
                            ) {
                                Log.e(
                                    FlutterUbiPlugin.TAG,
                                    "[UBICredentialProvider refreshPassportToken] error, code: $errorCode, message: $errorMessage, details: $errorDetails"
                                )
                                countDownLatch.countDown()
                            }

                            override fun notImplemented() {
                                countDownLatch.countDown()
                            }
                        })
                }
                countDownLatch.await()
                return newPassportToken
            }

        }
    }


}