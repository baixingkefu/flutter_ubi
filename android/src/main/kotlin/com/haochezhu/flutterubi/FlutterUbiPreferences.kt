package com.haochezhu.flutterubi

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by xian on 2022/4/22.
 */
class FlutterUbiPreferences {
    companion object {
        private const val PREFERENCE_NAME = "flutter_ubi_plugin"
        private const val KEY_REFRESH_PASSPORT_TOKEN_HANDLE = "KEY_REFRESH_PASSPORT_TOKEN_HANDLE"
        private const val KEY_CALLBACK_DISPATCHER_HANDLE = "KEY_CALLBACK_DISPATCHER_HANDLE"

        internal fun saveCallbackHandle(callbackDispatcherHandle: Long, refreshPassportTokenHandle: Long) {
            sharedPreferences().edit().apply {
                putLong(KEY_CALLBACK_DISPATCHER_HANDLE, callbackDispatcherHandle)
                putLong(KEY_REFRESH_PASSPORT_TOKEN_HANDLE, refreshPassportTokenHandle)
            }.apply()
        }

        internal fun getRefreshPassportTokenHandle() =
            sharedPreferences().getLong(KEY_REFRESH_PASSPORT_TOKEN_HANDLE, 0L)

        internal fun getCallbackDispatcherHandle() =
            sharedPreferences().getLong(KEY_CALLBACK_DISPATCHER_HANDLE, 0L)

        internal fun clearUserInfo() {
            sharedPreferences().edit().apply {
                putLong(KEY_CALLBACK_DISPATCHER_HANDLE, 0L)
                putLong(KEY_REFRESH_PASSPORT_TOKEN_HANDLE, 0L)
            }.apply()
        }

        private fun sharedPreferences(): SharedPreferences {
            return FlutterUbiPlugin.applicationContext.getSharedPreferences(
                PREFERENCE_NAME,
                Context.MODE_PRIVATE
            )
        }
    }
}