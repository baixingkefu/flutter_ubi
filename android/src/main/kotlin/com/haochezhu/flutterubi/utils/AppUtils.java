package com.haochezhu.flutterubi.utils;

import android.app.Application;

import com.haochezhu.flutterubi.FlutterUbiPlugin;

/**
 * Created by xian on 2022/4/24.
 */
public class AppUtils {

    public static Application getApp() {
        return (Application) FlutterUbiPlugin.Companion.getApplicationContext();
    }
}
