package com.haochezhu.flutterubi

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.annotation.NonNull
import com.blankj.utilcode.util.ProcessUtils
import com.haochezhu.ubm.api.ErrorCode
import com.haochezhu.ubm.api.UBICredentialProvider
import com.haochezhu.ubm.api.UBITracking
import com.haochezhu.ubm.data.GpsMsg
import com.haochezhu.ubm.ui.permission.PermissionWizard
import com.haochezhu.ubm.ui.permission.PermissionWizardActivity
import com.haochezhu.ubm.ui.triphistory.UbmTripHistorySumActivity
import com.haochezhu.ubm.ui.web.UbmWebActivity
import com.haochezhu.ubm.util.UbmLogUtils
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.util.concurrent.CountDownLatch

/** FlutterUbiPlugin */
class FlutterUbiPlugin : FlutterPlugin, ActivityAware, MethodCallHandler,
    EventChannel.StreamHandler {

    companion object {

        const val TAG: String = "FlutterUbi"

        lateinit var applicationContext: Context
            private set

        fun setContext(context: Context) {
            applicationContext = context.applicationContext
        }

        /**
         * 初始化FlutterUbiPlugin;该方法需要在Application.onCreate()中调用；
         */
        @JvmStatic
        fun init() {
            if (!ProcessUtils.isMainProcess()) {
                return
            }
            UbiSDKInitializer.getInstance().init()
        }

    }

    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel
    private lateinit var context: Context
    private var activity: Activity? = null

    private lateinit var eventChannel: EventChannel
    private var eventSink: EventChannel.EventSink? = null

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        context = flutterPluginBinding.applicationContext
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_ubi")
        channel.setMethodCallHandler(this)

        eventChannel = EventChannel(flutterPluginBinding.binaryMessenger, "speedChannel")
        eventChannel.setStreamHandler(this)

        EventBus.getDefault().register(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        Log.d(
            TAG,
            "[FlutterUbiPlugin onMethodCall] method: ${call.method}, args: ${call.arguments}"
        )
        when (call.method) {
            "initialize" -> {
                call.arguments<ArrayList<*>>()?.let {
                    val callbackDispatcherHandle = it[0] as Long
                    val refreshPassportTokenHandle = it[1] as Long
                    FlutterUbiPreferences.saveCallbackHandle(
                        callbackDispatcherHandle,
                        refreshPassportTokenHandle
                    )
                }
                result.success("")
            }
            "enableSDK" -> {
                val uid = call.argument<String>("userId")
                val token = call.argument<String>("passportToken")
                if (uid == null || token == null) {
                    result.error(
                        ErrorCode.invalidParam.toString(),
                        ErrorCode.errorMessage(ErrorCode.invalidParam),
                        null
                    )
                    return
                }
                UBITracking.enableSDK(uid, object : UBICredentialProvider(token) {
                    override fun refreshPassportToken(): String? = flutterRenewToken()
                }, onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
            }
            "disableSDK" -> {
                UBITracking.disableSDK(onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
                FlutterUbiPreferences.clearUserInfo()
            }
            "startTrip" -> {
                UBITracking.startTrip(onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
            }
            "quickStartTrip" -> {
                UBITracking.quickStartTrip(onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
            }
            "stopTrip" -> {
                UBITracking.stopTrip(onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
            }
            "startAuto" -> {
                UBITracking.startAuto(onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
            }
            "quickStartAuto" -> {
                UBITracking.quickStartAuto(onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
            }
            "stopAuto" -> {
                UBITracking.stopAuto(onSuccess = {
                    result.success("")
                }, onFailure = { code, msg ->
                    result.error(code.toString(), msg, null)
                })
            }
            "showWhiteListPage" -> {
                showWhiteListPage()
                result.success("")
            }
            "showPermissionPage" -> {
                showPermissionPage()
                result.success("")
            }
            "showTripHistoryPage" -> {
                showTripHistoryPage()
                result.success("")
            }
            "checkPermission" -> {
                result.success(checkPermission())
            }
            "isTripOnGoing" -> {
                result.success(UBITracking.isTripOnGoing())
            }
            "isAutoOn" -> {
                result.success(UBITracking.isAutoOn())
            }
            "isEnabled" ->{
                result.success(UBITracking.isEnabled())
            }
            "log" -> {
                val tag = call.argument<String>("tag") ?: "hostApp"
                val msg = call.argument<String>("msg") ?: ""
                UbmLogUtils.persistLog(tag, msg)
                result.success("")
            }
            else -> result.notImplemented()
        }

    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
        EventBus.getDefault().unregister(this)
    }

    private fun showWhiteListPage() {
        activity?.run {
            startActivity(UbmWebActivity.makeWebIntent(this))
        }
    }

    private fun showPermissionPage() {
        activity?.run {
            startActivity(Intent(this, PermissionWizardActivity::class.java))
        }
    }

    private fun showTripHistoryPage() {
        activity?.run {
            startActivity(Intent(this, UbmTripHistorySumActivity::class.java))
        }
    }

    private fun checkPermission() = PermissionWizard.checkPermissionGranted(context)

    private fun flutterRenewToken(): String? {
        val lock = CountDownLatch(1)
        var token: String? = null
        Handler(Looper.getMainLooper()).post {
            // 主线程调用flutter侧方法
            channel.invokeMethod("refreshPassportToken", null, object : MethodChannel.Result {
                override fun success(result: Any?) {
                    if (result is String)
                        token = result
                    lock.countDown()
                }

                override fun error(errorCode: String, errorMessage: String?, errorDetails: Any?) {
                    lock.countDown()
                }

                override fun notImplemented() {
                    lock.countDown()
                }
            })
        }
        lock.await()
        return token
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity
    }

    override fun onDetachedFromActivityForConfigChanges() {
        activity = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        activity = binding.activity
    }

    override fun onDetachedFromActivity() {
        activity = null
    }

    override fun onListen(arguments: Any?, events: EventChannel.EventSink?) {
        eventSink = events
    }

    override fun onCancel(arguments: Any?) {
        eventSink = null
    }

    @Subscribe
    fun onGpsMsg(msg: GpsMsg) {
        eventSink?.success("${(msg.gps.speed * 3.6).toInt()} km/h")
    }

}
