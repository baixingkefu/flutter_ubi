import 'package:flutter/material.dart';
import 'package:flutter_ubi/flutter_ubi.dart';
import 'package:flutter_ubi_example/page/login.dart';
import 'package:flutter_ubi_example/page/ubi.dart';

import 'common/sdk_utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final isSdkEnabled = await _initFlutterUbi();
  runApp(MyApp(isSdkEnabled));
}

Future<bool> _initFlutterUbi() async {
  final result = await FlutterUbi.init(requestUserPassportToken);
  return result.isSuccessful();
}

class MyApp extends StatelessWidget {
  final bool isSdkEnabled;

  const MyApp(this.isSdkEnabled, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: isSdkEnabled ? UBIPage() : LoginPage(),
    );
  }
}
