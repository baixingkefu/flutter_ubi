import 'package:flutter/material.dart';
import 'package:flutter_ubi/flutter_ubi.dart';
import 'package:flutter_ubi_example/common/app_preferences.dart';
import 'package:flutter_ubi_example/common/common_utils.dart';
import 'package:flutter_ubi_example/common/constant.dart';
import 'package:flutter_ubi_example/common/router.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("UBI Flutter SDK"),
      ),
      body: Center(
        child: ElevatedButton(
            child: Text("登录"),
            onPressed: () => {_onPressedLoginButton(context)}),
      ),
    );
  }

  void _onPressedLoginButton(BuildContext context) async {
    //登录的逻辑代码
    //
    // some logic code
    //

    if (Constants.AuthorizationCode.isEmpty ||
        Constants.AuthorizationCode == "your code") {
      final msg =
          "请在项目的example>lib>common>constant.dart文件中替换“your code”字样为你的授权码";
      context.showSnackBar(msg);
      return;
    }

    AppPreferences.getInstance().saveLoggedIn(true);

    //当登录成功后,调用需要调用Flutter.login()
    final result = await FlutterUbi.login();
    print("[FlutterUbiDemo login] result, code:${result.code}, msg:${result.msg}");

    if (result.isSuccessful()) {
      //初始化SDK成功之后，才能调用FlutterUbi
      AppRouter.startUbiPage(context);
    } else {
      context.showSnackBar("初始化SDK失败");
    }
  }
}
