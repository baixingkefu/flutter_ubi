import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ubi/flutter_ubi.dart';
import 'package:flutter_ubi/model.dart';
import 'package:flutter_ubi_example/common/app_preferences.dart';
import 'package:flutter_ubi_example/common/common_utils.dart';

import '../common/router.dart';

class UBIPage extends StatefulWidget {
  @override
  _UBIPageState createState() => new _UBIPageState();
}

class _UBIPageState extends State<UBIPage> {
  static const EventChannel _channel = const EventChannel('speedChannel');

  bool _showSpeed = false;
  String _platformMessage = '';
  StreamSubscription? _streamSubscription;

  late Map<String, Function(BuildContext context, String functionName)>
      _functions;

  _UBIPageState() {
    this._functions = {
      '打开白名单引导页面': (BuildContext context, String functionName) {
        FlutterUbi.showWhiteListPage();
      },
      '打开权限申请页面': (BuildContext context, String functionName) {
        FlutterUbi.showPermissionPage();
      },
      '开始手动行程': (BuildContext context, String functionName) async {
        final ubiResult = await FlutterUbi.startTrip();
        _showUBIResult(context, functionName, ubiResult);
      },
      '快速开始手动行程': (BuildContext context, String functionName) async {
        final ubiResult = await FlutterUbi.quickStartTrip();
        _showUBIResult(context, functionName, ubiResult);
      },
      '结束手动行程': (BuildContext context, String functionName) async {
        final ubiResult = await FlutterUbi.stopTrip();
        _showUBIResult(context, functionName, ubiResult);
      },
      '打开自动监测': (BuildContext context, String functionName) async {
        final ubiResult = await FlutterUbi.startAuto();
        _showUBIResult(context, functionName, ubiResult);
      },
      '快速打开自动监测': (BuildContext context, String functionName) async {
        final ubiResult = await FlutterUbi.quickStartAuto();
        _showUBIResult(context, functionName, ubiResult);
      },
      '关闭自动监测': (BuildContext context, String functionName) async {
        final ubiResult = await FlutterUbi.stopAuto();
        _showUBIResult(context, functionName, ubiResult);
      },
      '打开历史行程页面': (BuildContext context, String functionName) {
        FlutterUbi.showTripHistoryPage();
      },
      '登出': (BuildContext context, String functionName) async {
        final ubiResult = await FlutterUbi.logout();
        _showUBIResult(context, functionName, ubiResult);
        AppPreferences.getInstance().saveLoggedIn(false);
        AppRouter.startLoginPage(context);
      }
    };
  }

  @override
  void initState() {
    super.initState();
    _enableEventReceiver();
  }

  void _enableEventReceiver() {
    _streamSubscription = _channel.receiveBroadcastStream().listen((event) {
      _showSpeed = true;
      print('接受到的数据' + event);
      setState(() {
        _platformMessage = event;
      });
    }, onError: (error) {
      print('错误' + error);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _streamSubscription?.cancel();
    _streamSubscription = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("UBI Flutter SDK"),
        actions: [],
      ),
      backgroundColor: Color(0xffF7F7F7),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Visibility(
            child: Container(
              padding: EdgeInsets.only(top: 30),
              child: Text('当前速度:$_platformMessage'),
            ),
            visible: _showSpeed,
          ),
          Expanded(
            child: _buildFunctionsWidget(context),
            flex: 1,
          )
        ],
      ),
    );
  }

  Widget _buildFunctionsWidget(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      childAspectRatio: 2,
      children: _functions.keys.map((functionName) {
        return Center(
          child: ElevatedButton(
            child: Text(functionName),
            onPressed: () {
              _functions[functionName]!(context, functionName);
            },
          ),
        );
      }).toList(),
    );
  }

  Future<bool> checkEnabled(BuildContext context) async {
    final isEnabled = await FlutterUbi.isEnabled();
    print(
        "[FlutterUbi checkEnabled] SDK ${isEnabled ? "is" : "isn't"} enabled");
    if (!isEnabled) {
      context.showSnackBar("请先初始化SDK");
    }
    return isEnabled;
  }

  void _showUBIResult(
      BuildContext context, String functionName, UBIResult ubiResult) {
    final resultText =
        "$functionName ${ubiResult.isSuccessful() ? "成功" : "失败, ${ubiResult.msg}"}";
    if (functionName == '结束手动行程' || functionName == '关闭SDK') {
      setState(() {
        _showSpeed = false;
      });
    }
    context.showSnackBar(resultText);
  }
}
