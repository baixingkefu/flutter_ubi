import 'package:flutter/material.dart';

extension BuildContextExt on BuildContext {
  void showSnackBar(String msg) {
    final snackBar = SnackBar(content: Text(msg), duration: Duration(seconds: 2),);
    ScaffoldMessenger.of(this).showSnackBar(snackBar);
  }
}
