import 'dart:io';

import 'package:shared_preferences_android/shared_preferences_android.dart';
import 'package:shared_preferences_ios/shared_preferences_ios.dart';

class SharedPreferencesWorkaround {
  SharedPreferencesWorkaround._();

  static void register() {
    if (Platform.isAndroid) SharedPreferencesAndroid.registerWith();
    if (Platform.isIOS) SharedPreferencesIOS.registerWith();
  }
}
