import 'package:dio/dio.dart';
import 'package:flutter_ubi/model.dart';
import 'package:flutter_ubi_example/common/app_preferences.dart';
import 'package:flutter_ubi_example/common/constant.dart';

Future<UserPassportToken?> requestUserPassportToken() async {
  if (!(await AppPreferences.getInstance().isLoggedIn())) {
    print("[FlutterUbiDemo requestUserPassportToken] App isn't logged in");
    return null;
  }
  try {
    final response = await Dio().get(Constants.RequestAccessTokenUrl);
    final statusCode = response.statusCode ?? -1;
    print("[FlutterUbiDemo requestUserPassportToken] http result, statusCode: $statusCode");
    if (statusCode >= 200 && statusCode < 300) {
      var ubiUserId = response.data['uid'].toString();
      var ubiPassportToken = response.data['token'].toString();
      return UserPassportToken(ubiUserId, ubiPassportToken);
    }
  } on DioError catch (e) {
    print("[FlutterUbiDemo requestUserPassportToken] error:${e.error}, message:${e.message}, stackTrack:${e.stackTrace}");
  }
  return null;
}
