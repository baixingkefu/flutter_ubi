class Constants {
  static const AuthorizationCode = "your code";
  static const UbiUserIdKey = 'UBIUserIdUDKey';
  static const UbiPassportTokenKey = 'UBIPassportTokenKey';

  static const Host = "https://driver-behavior-api.test.haochezhu.club";
  static const RequestAccessTokenUrl =
      Host + "/v1/access-token/$AuthorizationCode";

  Constants._();
}
