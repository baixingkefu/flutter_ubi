import 'package:flutter/material.dart';
import 'package:flutter_ubi_example/page/login.dart';

import '../page/ubi.dart';

class AppRouter {
  AppRouter._();

  static void startUbiPage(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => UBIPage()),
        (Route? route) => false);
  }

  static void startLoginPage(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
        (Route? route) => false);
  }
}
