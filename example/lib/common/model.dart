class Pair<T1, T2> {
  final T1 first;
  final T2 second;

  Pair(this.first, this.second);

  @override
  String toString() => 'Pair[$first, $second]';
}

class User{
  final String id;
  final String passportToken;

  User(this.id, this.passportToken);

  @override
  String toString() {
    return 'UserInfo{id: $id, passportToken: $passportToken}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is User &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          passportToken == other.passportToken;

  @override
  int get hashCode => id.hashCode ^ passportToken.hashCode;
}