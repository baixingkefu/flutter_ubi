import 'package:flutter_ubi_example/common/shared_preferences_workground.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static const KEY_LOGGED_IN = "loggedIn";

  static AppPreferences? _instance;

  AppPreferences._() {
    SharedPreferencesWorkaround.register();
  }

  static AppPreferences getInstance() {
    if (_instance == null) {
      _instance = AppPreferences._();
    }
    return _instance!;
  }

  Future<void> saveLoggedIn(bool loggedIn) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool(KEY_LOGGED_IN, loggedIn);
  }

  Future<bool> isLoggedIn() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getBool(KEY_LOGGED_IN) ?? false;
    return result;
  }
}
