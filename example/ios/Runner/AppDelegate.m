#import "AppDelegate.h"
#import "GeneratedPluginRegistrant.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//    FlutterViewController* controller = (FlutterViewController *)self.window.rootViewController;
    
//    FlutterMethodChannel* batteryChannel = [FlutterMethodChannel
//                                            methodChannelWithName:@""
//                                            binaryMessenger:controller.binaryMessenger];
//
//    [batteryChannel setMethodCallHandler:^(FlutterMethodCall* call, FlutterResult result) {
//        // Note: this method is invoked on the UI thread.
//        // TODO
//    }];
    
    [GeneratedPluginRegistrant registerWithRegistry:self];
    // Override point for customization after application launch.
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

//- (nonnull NSObject<FlutterBinaryMessenger> *)messenger {
//    return self;
//}

@end
