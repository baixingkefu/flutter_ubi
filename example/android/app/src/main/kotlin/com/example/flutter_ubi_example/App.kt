package com.example.flutter_ubi_example

import android.app.Application
import com.haochezhu.flutterubi.FlutterUbiPlugin

/**
 * Created by xian on 2022/4/22.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        FlutterUbiPlugin.init()
    }
}