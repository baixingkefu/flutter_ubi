cd "$(dirname "$0")"
flutter clean
rm -rf pubspec.lock
rm -rf build
rm -rf ios/Podfile.lock
rm -rf ios/Pods
rm -rf ios/.symlinks
rm -rf ${HOME}/Library/Developer/Xcode/DerivedData
flutter pub get