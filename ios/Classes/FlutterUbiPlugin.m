#import "FlutterUbiPlugin.h"

static FlutterMethodChannel *channel;
static FlutterEventChannel *eventChannel;

@interface FlutterUbiPlugin () <UBICredentialProviderDelegate, FlutterStreamHandler> {
    FlutterEventSink _eventSink;
}
@property(nonatomic, strong) FlutterMethodChannel *channel;

@end

@implementation FlutterUbiPlugin
+ (void)registerWithRegistrar:(NSObject <FlutterPluginRegistrar> *)registrar {
    channel = [FlutterMethodChannel
            methodChannelWithName:@"flutter_ubi"
                  binaryMessenger:[registrar messenger]];
    eventChannel = [FlutterEventChannel eventChannelWithName:@"speedChannel" binaryMessenger:[registrar messenger]];

    FlutterUbiPlugin *instance = [[FlutterUbiPlugin alloc] init];
    [registrar addMethodCallDelegate:instance channel:channel];
    instance.channel = channel;
    [eventChannel setStreamHandler:instance];
    [[NSNotificationCenter defaultCenter] addObserver:instance selector:@selector(speedCallback:) name:@"UBMUIdataUpdateNotification" object:nil];
}

- (FlutterError *)onListenWithArguments:(id)arguments eventSink:(FlutterEventSink)events {
    _eventSink = events;
    return nil;
}

- (FlutterError *)onCancelWithArguments:(id)arguments {
    _eventSink = nil;
    return nil;
}

- (void)speedCallback:(NSNotification *)notify {
    UBMTipUIModel *uimodel = notify.object;
    if (uimodel) {
        NSString *speed = [NSString stringWithFormat:@"%@km/h", uimodel.speed];
        if (_eventSink) {
            _eventSink(speed);
        }
    }
}

- (void)handleMethodCall:(FlutterMethodCall *)call result:(FlutterResult)result {
    NSString *logMsg = [NSString stringWithFormat:@"handleMethodCall, method : %@, arguments : %@", call.method, call.arguments];
    [UBITracking.shared addLog:@"FlutterUbiPlugin" andMsg:logMsg];

    if ([@"enableSDK" isEqualToString:call.method]) {
        NSString *userId = call.arguments[@"userId"];
        NSString *passportToken = call.arguments[@"passportToken"];
        if (userId == NULL || passportToken == NULL) {
            FlutterError *error = [FlutterError errorWithCode:@"1000" message:@"调用参数错误" details:nil];
            result(error);
            return;
        }
        [UBITracking.shared enableSDK:userId provider:[[UBICredentialProvider alloc] initWithPassportToken:passportToken delegate:self] onSuccess:^{
            result(@"");
        }                   onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"disableSDK" isEqualToString:call.method]) {

        [UBITracking.shared disableSDKOnSuccess:^{
            result(@"");
        }                             onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"startTrip" isEqualToString:call.method]) {
        [UBITracking.shared startTripOnSuccess:^{
            result(@"");
        }                            onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"stopTrip" isEqualToString:call.method]) {
        [UBITracking.shared stopTripOnSuccess:^{
            result(@"");
        }                           onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"startAuto" isEqualToString:call.method]) {
        [UBITracking.shared startAutoRecordOnSuccess:^{
            result(@"");
        }                                  onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"quickStartAuto" isEqualToString:call.method]) {
        [UBITracking.shared quickStartAutoRecordOnSuccess:^{
            result(@"");
        }                                       onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"quickStartTrip" isEqualToString:call.method]) {
        [UBITracking.shared quickStartTripOnSuccess:^{
            result(@"");
        }                                 onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"stopAuto" isEqualToString:call.method]) {
        [UBITracking.shared stopAutoRecordOnSuccess:^{
            result(@"");
        }                                 onFailure:^(NSInteger code, NSString *_Nonnull msg) {
            FlutterError *error = [FlutterError errorWithCode:[NSString stringWithFormat:@"%ld", (long) code] message:msg details:nil];
            result(error);
        }];

    } else if ([@"showWhiteListPage" isEqualToString:call.method]) {
        [self showWhiteListPageWithCompletion:^{
            result(@"");
        }];

    } else if ([@"showPermissionPage" isEqualToString:call.method]) {
        [self showPermissionPageWithCompletion:^{
            result(@"");
        }];

    } else if ([@"showTripHistoryPage" isEqualToString:call.method]) {
        [self showTripHistoryPageWithCompletion:^{
            result(@"");
        }];

    } else if ([@"checkPermission" isEqualToString:call.method]) {
        BOOL ret = [UBMPermision hasLocationPermission] && [UBMPermision hasHealthServicesPermission];
        NSNumber *num = [NSNumber numberWithBool:ret];
        result(num);

    } else if ([@"isTripOnGoing" isEqualToString:call.method]) {
        BOOL ret = [UBITracking.shared isTripOnGoing];
        NSNumber *num = [NSNumber numberWithBool:ret];
        result(num);
        
    } else if ([@"isAutoOn" isEqualToString:call.method]) {
        BOOL ret = [UBITracking.shared isAutoOn];
        NSNumber *num = [NSNumber numberWithBool:ret];
        result(num);

    } else if ([@"log" isEqualToString:call.method]) {
        NSString *tag = call.arguments[@"tag"];
        NSString *msg = call.arguments[@"msg"];
        [UBITracking.shared addLog:tag andMsg:msg];
        result(@"");

    } else if ([@"isEnabled" isEqualToString:call.method]) {
        BOOL ret = [UBITracking.shared isEnabled];
        NSNumber *num = [NSNumber numberWithBool:ret];
        result(num);

    } else {
        result(FlutterMethodNotImplemented);
    }
}

- (void)test {

}

- (void)showWhiteListPageWithCompletion:(void (^ __nullable)(void))completion {
    NSLog(@"跳转到iOS页面");
    UBMAuthGuideController *guideVc = [[UBMAuthGuideController alloc] init];
    guideVc.fromFlutter = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:guideVc];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:NO completion:completion];
}

- (void)showPermissionPageWithCompletion:(void (^ __nullable)(void))completion {
    UBMAuthSettingController *settingVc = [[UBMAuthSettingController alloc] init];
    settingVc.fromFlutter = YES;
    settingVc.hidesBottomBarWhenPushed = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:settingVc];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:NO completion:completion];
}

- (void)showTripHistoryPageWithCompletion:(void (^ __nullable)(void))completion {
    UBMRouteHistoryController *historyVc = [[UBMRouteHistoryController alloc] init];
    historyVc.hidesBottomBarWhenPushed = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:historyVc];
    nav.modalPresentationStyle = UIModalPresentationFullScreen;
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:nav animated:false completion:completion];
}

- (NSString *_Nullable)UBICredentialRefreshPassportToken {
    __block NSString *ubiToken = NULL;
    __block dispatch_semaphore_t signal = dispatch_semaphore_create(0);
    [channel invokeMethod:@"refreshPassportToken" arguments:NULL result:^(id _Nullable result) {
        ubiToken = result;
        dispatch_semaphore_signal(signal);
    }];

    intptr_t ret = dispatch_semaphore_wait(signal, DISPATCH_TIME_FOREVER);
    if (!ret) { // 没有超时
        NSLog(@"UBICredential 没有超时");
    } else { // 超时
        NSLog(@"UBICredential 超时");
    }

    return ubiToken;
}

@end
