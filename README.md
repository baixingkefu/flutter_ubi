# flutter_ubi
百姓车联 UBI SDK 提供驾驶数据收集与分析功能，可以帮助企业洞察用户的驾驶习惯。同时 UBI SDK 提供多种基于用户驾驶行为的运营活动，从而帮助企业更频繁地与用户互动，改善用户的驾驶习惯。

企业用户可以将 UBI Flutter SDK 接入到自己的 APP 中，快速获取收集与分析用户驾驶行为的能力。您可以参照 [UBI Flutter SDK 接入指南](https://driver-behavior-api.haochezhu.club/docs/flutter/guide.html) 中的指引完成 UBI Flutter SDK 的接入。

UBI Flutter SDK 目前支持 Android 和 iOS 平台。

