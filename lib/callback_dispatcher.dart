import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_ubi/model.dart';

void callbackDispatcher() {
  WidgetsFlutterBinding.ensureInitialized();
  const MethodChannel _methodChannel =
      MethodChannel("flutter_ubi_refresh_passport_token");
  _methodChannel.setMethodCallHandler((call) async {
    print("[FlutterUbi callbackDispatcher] onMethodCall, method:${call.method}, args:${call.arguments}");
    switch (call.method) {
      case "refreshPassportToken":
        final List<dynamic> args = call.arguments;
        final Function? refreshUserPassportToken =
            PluginUtilities.getCallbackFromHandle(
                CallbackHandle.fromRawHandle(args[0]));
        if (refreshUserPassportToken == null) {
          print("[FlutterUbi callbackDispatcher] onMethodCall, function is null");
          return null;
        }
        print("[FlutterUbi callbackDispatcher] onMethodCall, call refreshUserPassportToken");
        final UserPassportToken? userPassportToken = await refreshUserPassportToken();
        print("[FlutterUbi callbackDispatcher] onMethodCall, result:$userPassportToken");
        return userPassportToken?.toMap();
    }
  });
  _methodChannel.invokeMethod("initialized");
}
