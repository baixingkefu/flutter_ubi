class UBIResult {
  final int code;
  final String? msg;

  UBIResult(this.code, {this.msg});

  UBIResult.success() : this(ErrorCode.success);

  UBIResult.failure(String code, String? msg)
      : this(int.tryParse(code) ?? ErrorCode.unknownError, msg: msg);

  bool isSuccessful() => code == ErrorCode.success;
}

class ErrorCode {
  ///成功
  static final int success = 0;

  ///未知错误
  static final int unknownError = 1;

  ///无效的参数
  static final int invalidParam = 1000;

  ///sdk没有初始化
  static final int sdkNotInitialized = 2000;

  ///没有权限
  static final int noPermissions = 3000;

  //上传pb文件失败
  static final int uploadPbFailed = 4001;

  ///没有运行中的行程，无法关闭行程
  static final int noRunningTrip = 4002;

  //在非主进程上运行
  static final int notRunInMainProcess = 4003;
}

class UserPassportToken {
  final String userId;
  final String passportToken;

  UserPassportToken(this.userId, this.passportToken);

  Map<String, String> toMap() {
    return {"userId": userId, "passportToken": passportToken};
  }
}
