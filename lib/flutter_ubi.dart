import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'callback_dispatcher.dart';
import 'model.dart';

//    Copyright (c) [2022] [百姓车联]
//    [Flutter UBI] is licensed under Mulan PSL v2.
//    You can use this software according to the terms and conditions of the Mulan PSL v2.
//    You may obtain a copy of Mulan PSL v2 at:
//             http://license.coscl.org.cn/MulanPSL2
//    THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
//    See the Mulan PSL v2 for more details.

typedef RefreshPassportToken = Future<UserPassportToken?> Function();

DateFormat millisecondFormat = DateFormat('yyyy-MM-dd HH:MM:ss:SSS');

class FlutterUbi {
  FlutterUbi._();

  static const MethodChannel _channel = MethodChannel('flutter_ubi');
  static RefreshPassportToken? _refreshPassportToken;

  ///初始化SDK;
  ///需要在 main() 中调用；
  ///当APP启动、用户登录时，会在后台调用RefreshUserPassportToken获取 ubi user id 和 ubi passport token；
  static Future<UBIResult> init(RefreshPassportToken refresh) async {
    FlutterUbi._refreshPassportToken = refresh;
    if (Platform.isAndroid) {
      _registerCallbackHandle(refresh);
    }
    return await _tryToEnableSDK(refresh);
  }

  static Future<UBIResult> _tryToEnableSDK(RefreshPassportToken refresh) async {
    if (await isEnabled()) {
      return UBIResult.success();
    }

    final result = await refresh();
    if (result == null ||
        result.userId.isEmpty ||
        result.passportToken.isEmpty) {
      return UBIResult(ErrorCode.invalidParam,
          msg: "userId or passportToken is empty");
    }

    return enableSDK(
        userId: result.userId,
        passportToken: result.passportToken,
        refresh: refresh);
  }

  static Future<UBIResult> _registerCallbackHandle(
      RefreshPassportToken refresh) async {
    int? refreshPassportTokenHandle =
        PluginUtilities.getCallbackHandle(refresh)?.toRawHandle();
    if (refreshPassportTokenHandle == null) {
      print(
          '[FlutterUbi init] ERROR: Failed to get callback handle Check whatever the refresh is a op-level or static function');
      return UBIResult(ErrorCode.invalidParam,
          msg: "The refresh is not a op-level or static function");
    }
    final callbackDispatcherHandle =
        PluginUtilities.getCallbackHandle(callbackDispatcher)?.toRawHandle();
    try {
      await _channel.invokeMethod(
          "initialize", [callbackDispatcherHandle, refreshPassportTokenHandle]);
      return UBIResult.success();
    } on PlatformException catch (e) {
      print("[FlutterUbi initialize] Error: ${e.message}");
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///当用户登录的时候调用该方法;
  static Future<UBIResult> login() async {
    if (_refreshPassportToken == null) {
      return UBIResult(ErrorCode.unknownError,
          msg: "Call init() in main() first");
    }
    return await _tryToEnableSDK(_refreshPassportToken!);
  }

  ///当用户登出时调用该方法；
  static Future<UBIResult> logout() {
    return disableSDK();
  }

  ///初始化SDK（开启SDK），refreshPassportToken : 刷新token的方法
  static Future<UBIResult> enableSDK(
      {required String userId,
      required String passportToken,
      required RefreshPassportToken refresh}) async {
    _channel.setMethodCallHandler((call) async {
      print(
          "[FlutterUbi enableSDK onMethodCall] method:${call.method}, args:${call.arguments}");
      String method = call.method;
      //刷新token
      if (method == 'refreshPassportToken') {
        final userPassportToken = await refresh();
        return userPassportToken?.passportToken ?? "";
      }
      return true;
    });

    try {
      await _channel.invokeMethod(
          'enableSDK', {"userId": userId, "passportToken": passportToken});
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///关闭SDK
  static Future<UBIResult> disableSDK() async {
    _channel.setMethodCallHandler(null);
    try {
      await _channel.invokeMethod('disableSDK');
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///开始手动行程
  static Future<UBIResult> startTrip() async {
    try {
      await _channel.invokeMethod('startTrip');
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///快速开启手动行程
  static Future<UBIResult> quickStartTrip() async {
    try {
      await _channel.invokeMethod("quickStartTrip");
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///结束手动行程
  static Future<UBIResult> stopTrip() async {
    try {
      await _channel.invokeMethod('stopTrip');
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///打开自动监测
  static Future<UBIResult> startAuto() async {
    try {
      await _channel.invokeMethod("startAuto");
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///快速打开自动监测
  static Future<UBIResult> quickStartAuto() async {
    try {
      await _channel.invokeMethod("quickStartAuto");
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///关闭自动监测
  static Future<UBIResult> stopAuto() async {
    try {
      await _channel.invokeMethod("stopAuto");
      return UBIResult.success();
    } on PlatformException catch (e) {
      return UBIResult.failure(e.code, e.message);
    }
  }

  ///打开白名单/权限引导页面
  static Future<void> showWhiteListPage() async {
    await _channel.invokeMethod("showWhiteListPage");
  }

  ///打开权限设置页面
  static Future<void> showPermissionPage() async {
    await _channel.invokeMethod("showPermissionPage");
  }

  ///打开历史行程页面
  static Future<void> showTripHistoryPage() async {
    await _channel.invokeMethod("showTripHistoryPage");
  }

  ///检查是否授予了开启行程所必须的权限
  static Future<bool> checkPermission() async {
    return await _channel.invokeMethod('checkPermission');
  }

  ///是否开启了自动记录
  static Future<bool> isAutoOn() async {
    return await _channel.invokeMethod('isAutoOn');
  }

  ///是否开始了自动记录
  static Future<bool> isTripOnGoing() async {
    return await _channel.invokeMethod('isTripOnGoing');
  }

  static Future<bool> isEnabled() async {
    return await _channel.invokeMethod("isEnabled");
  }

  static Future<void> log(String tag, String msg) async {
    final newMsg = "$msg; logTime:" + millisecondFormat.format(DateTime.now());
    return await _channel.invokeMethod("log", {"tag": tag, "msg": newMsg});
  }
}
